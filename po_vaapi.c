#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "po_vaapi.h"
#include "po_vaapi_bitstream.h"

#define CHECK_VASTATUS(status, func, ...) do {\
        status = func(__VA_ARGS__); \
        if (status != VA_STATUS_SUCCESS) { \
                printf("error at %s %s %d status=%x '%s'\n", \
                        #func, __func__, __LINE__, status, vaErrorStr(status));\
                return status; \
        }\
} while (0)

/******************************************************************************
 * VAAPI Window System context
 *****************************************************************************/
po_vaapi_ctx *
po_vaapi_alloc(Display *display)
{
        int opened_display = 0;
        po_vaapi_ctx *ctx = 0;
        VADisplay *va_display = NULL;
        int major = 0, minor = 0;
        ctx = malloc(sizeof(po_vaapi_ctx));

        if (!ctx) {
                goto err;
        }
        memset(ctx, 0, sizeof(po_vaapi_ctx));

        if (!display) {
                display = XOpenDisplay(NULL);
                if (display) {
                        opened_display = 1;
                }
                else {
                        goto err;
                }
        }

        va_display = vaGetDisplay(display);
        if (!va_display || !vaDisplayIsValid(va_display)) {
                goto err;
        }

        if (vaInitialize(va_display, &major, &minor) != VA_STATUS_SUCCESS) {
                goto err;
        }

        if (opened_display) {
                ctx->display = display;
        }

        ctx->va_display = va_display;
        ctx->va_version_major = major;
        ctx->va_version_minor = minor;

        return ctx;
err:
        if (va_display) {
                vaTerminate(va_display);
        }
        if (display && opened_display) {
                XCloseDisplay(display);
        }
        if (ctx) {
                free(ctx);
        }
        return NULL;
}

void
po_vaapi_free(po_vaapi_ctx *ctx)
{
        if (!ctx) {
                return;
        }

        if (ctx->va_display) {
                vaTerminate(ctx->va_display);
        }
        if (ctx->display) {
                XCloseDisplay(ctx->display);
        }
        free(ctx);
}

/******************************************************************************
 * Encoding context
 *****************************************************************************/
#define MB_ALIGN(num) (((num) + 15) & ~15)
#define TO_MBS(num) (MB_ALIGN((num)) / 16)

#define ROUND_TO_MULT(num, mult) (((num) + (mult) - 1) / (mult))

VAStatus
po_vaapi_enc264_fixup_params(po_vaapi_enc264_params *params)
{
        if (!params) {
                return VA_STATUS_ERROR_INVALID_VALUE;
        }
        /* XXX: parse config here */
        params->width_in_mbs = TO_MBS(params->width);
        params->height_in_mbs = TO_MBS(params->height);

        if ((!params->width_in_mbs) || (!params->height_in_mbs)) {
                return VA_STATUS_ERROR_INVALID_VALUE;
        }

        if (!params->frame_rate) {
                params->frame_rate = 25;
        }
        if (!params->intra_period) {
                params->intra_period = 5 * params->frame_rate;
        }
        if (!params->time_scale) {
                params->time_scale = 900;
        }
        if (!params->num_units_in_tick) {
                params->num_units_in_tick = 15;
        }
        if (!params->level_idc) {
                params->level_idc = 41;
        }

        switch (params->rate_control_method) {
                case VA_RC_CQP:
                        if (!params->init_qp) {
                                params->init_qp = 20;
                        }
                        params->target_bitrate = -1;
                        break;
                //case VA_RC_CBR: //FIXME: currently produces random garbage
                case VA_RC_VBR:
                        if (!params->target_bitrate) {
                                params->target_bitrate = 14 * 1000 * 1000;
                        }
                        break;
                default:
                        return VA_STATUS_ERROR_INVALID_VALUE;
        }

        switch (params->va_profile) {
                case VAProfileH264ConstrainedBaseline:
                case VAProfileH264Baseline:
                        params->constraint_set_flag |= (1 << 0);
                        break;
                case VAProfileH264Main:
                        params->constraint_set_flag |= (1 << 1);
                        break;
                case VAProfileH264High:
                        params->constraint_set_flag |= (1 << 3);
                        break;
                default:
                        return VA_STATUS_ERROR_INVALID_VALUE;
                        break;
        }

        return VA_STATUS_SUCCESS;
}

VAStatus
po_vaapi_enc264_alloc(po_vaapi_enc264 **p_encoder,
        po_vaapi_ctx *context,
        po_vaapi_enc264_params *input_params)
{
        po_vaapi_enc264 *encoder = NULL;
        po_vaapi_enc264_params *params = NULL;
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;

        if ((NULL == p_encoder) || (NULL == context) || (NULL == input_params)) {
                goto fail;
        }

        CHECK_VASTATUS(status, po_vaapi_enc264_fixup_params, input_params);

        encoder = (po_vaapi_enc264*)malloc(sizeof(po_vaapi_enc264));
        if (!encoder) {
                goto fail;
        }
        memset(encoder, 0, sizeof(po_vaapi_enc264));

        encoder->po_context = context;
        encoder->params = *input_params;
        params = &encoder->params;
        encoder->current_input_surface = IDX_INPUT_0,

        encoder->coded_buffer_size = (params->width * params->height * 4);
        encoder->seq_param_buf_id = VA_INVALID_ID;
        encoder->pic_param_buf_id = VA_INVALID_ID;
        encoder->slice_param_buf_id = VA_INVALID_ID;
        encoder->misc_parameter_hrd_buf_id = VA_INVALID_ID;

        encoder->packed_seq_header_param_buf_id = VA_INVALID_ID;
        encoder->packed_seq_buf_id = VA_INVALID_ID;
        encoder->packed_pic_header_param_buf_id = VA_INVALID_ID;
        encoder->packed_pic_buf_id = VA_INVALID_ID;
        encoder->packed_sei_buf_id = VA_INVALID_ID;
        encoder->packed_sei_header_buf_id = VA_INVALID_ID;

        VAConfigAttrib attrib[2];
        attrib[0].type = VAConfigAttribRTFormat;
        attrib[0].value = VA_RT_FORMAT_YUV420;
        attrib[1].type = VAConfigAttribRateControl;
        attrib[1].value = params->rate_control_method;

        CHECK_VASTATUS(status, vaCreateConfig,
                context->va_display, params->va_profile,
                VAEntrypointEncSlice, attrib, 2, &encoder->va_config);

        VASurfaceAttrib attribs[] = {
                {
                        .type = VASurfaceAttribPixelFormat,
                        .flags = VA_SURFACE_ATTRIB_SETTABLE,
                        .value = {
                                .type = VAGenericValueTypeInteger,
                                .value.i = VA_FOURCC_NV12,
                        }
                }
        };

        CHECK_VASTATUS(status, vaCreateSurfaces, context->va_display,
                VA_RT_FORMAT_YUV420,
                params->width, params->height,
                encoder->va_surfaces, N_SURFACES,
                attribs, 1);

        CHECK_VASTATUS(status, vaCreateContext,
                context->va_display, encoder->va_config,
                params->width, params->height,
                VA_PROGRESSIVE, 0, 0, &encoder->va_context);

        CHECK_VASTATUS(status, vaCreateBuffer, context->va_display,
                encoder->va_context, VAEncCodedBufferType,
                encoder->coded_buffer_size, 1, NULL, &encoder->coded_buffer);

        CHECK_VASTATUS(status, vaCreateBuffer, context->va_display,
                encoder->va_context, VAEncSequenceParameterBufferType,
                sizeof(VAEncSequenceParameterBufferH264), 1,
                &encoder->seq_params, &encoder->seq_param_buf_id);

        CHECK_VASTATUS(status, vaCreateBuffer, context->va_display,
                encoder->va_context, VAEncPictureParameterBufferType,
                sizeof(VAEncPictureParameterBufferH264), 1,
                &encoder->pic_params, &encoder->pic_param_buf_id);

        CHECK_VASTATUS(status, vaCreateBuffer, context->va_display,
                encoder->va_context, VAEncSliceParameterBufferType,
                sizeof(VAEncSliceParameterBufferH264), 1,
                &encoder->slice_params, &encoder->slice_param_buf_id);

        if (params->rate_control_method != VA_RC_CBR) {
                goto done;
        }

        CHECK_VASTATUS(status, po_vaapi_build_packed_sei,
                &encoder->bitstream_sei, &encoder->params);

        params->init_cpb_removal_delay =
                (((params->target_bitrate * 8) >> 10) * 512) /
                (params->target_bitrate * 90000);
        params->init_cpb_removal_length = 24;
        params->cpb_removal_delay = 2;
        params->cpb_removal_length = 24;
        params->dpb_output_length = 24;

        encoder->packed_header_params = (VAEncPackedHeaderParameterBuffer) {
            .type = VAEncPackedHeaderH264_SEI,
            .bit_length = encoder->bitstream_sei->bit_offset,
        };

        CHECK_VASTATUS(status, vaCreateBuffer, context->va_display,
                encoder->va_context, VAEncPackedHeaderParameterBufferType,
                sizeof(VAEncPackedHeaderParameterBuffer), 1,
                &encoder->packed_header_params,
                &encoder->packed_sei_header_buf_id);

        CHECK_VASTATUS(status, vaCreateBuffer, context->va_display, encoder->va_context,
                VAEncPackedHeaderDataBufferType,
                ROUND_TO_MULT(encoder->bitstream_sei->bit_offset, 8), 1,
                encoder->bitstream_sei->buffer,
                &encoder->packed_sei_buf_id);

        CHECK_VASTATUS(status, vaCreateBuffer, context->va_display, encoder->va_context,
                VAEncMiscParameterBufferType,
                sizeof(VAEncMiscParameterBuffer) + sizeof(VAEncMiscParameterRateControl),
                1, 0, &encoder->misc_parameter_hrd_buf_id);

        VAEncMiscParameterBuffer *misc_param = NULL;
        VAEncMiscParameterHRD *misc_hrd_param = NULL;
        CHECK_VASTATUS(status, vaMapBuffer, context->va_display, encoder->misc_parameter_hrd_buf_id,
                (void**)&misc_param);

        misc_param->type = VAEncMiscParameterTypeHRD;
        misc_hrd_param = (VAEncMiscParameterHRD*)misc_param->data;

        if (params->target_bitrate > 0) {
                misc_hrd_param->initial_buffer_fullness = params->target_bitrate * 1024 * 4;
                misc_hrd_param->buffer_size = params->target_bitrate * 1024 * 8;
        } else {
                misc_hrd_param->initial_buffer_fullness = 0;
                misc_hrd_param->buffer_size = 0;
        }

        CHECK_VASTATUS(status, vaUnmapBuffer, context->va_display, encoder->misc_parameter_hrd_buf_id);

done:
        *p_encoder = encoder;
        return VA_STATUS_SUCCESS;

fail:
        if (NULL != encoder) {
                free(encoder);
        }
        return status;
}

static VAStatus
po_vaapi_update_buffer(po_vaapi_ctx *ctx,
        VABufferID buffer, void *data, size_t length)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;

        void *buffer_ptr = NULL;
        CHECK_VASTATUS(status, vaMapBuffer, ctx->va_display, buffer, &buffer_ptr);
        memcpy(buffer_ptr, data, length);
        CHECK_VASTATUS(status, vaUnmapBuffer, ctx->va_display, buffer);

        return VA_STATUS_SUCCESS;
}

static VAStatus
po_vaapi_create_or_update_buffer(po_vaapi_ctx *ctx, VAContextID va_context,
        VABufferID *buffer_id, VABufferType type, void *data, size_t length)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;

        if (VA_INVALID_ID == (*buffer_id)) {
                CHECK_VASTATUS(status, vaCreateBuffer,
                        ctx->va_display, va_context,
                        type, length, 1, data, buffer_id);
        }
        else {
                CHECK_VASTATUS(status, po_vaapi_update_buffer,
                        ctx, *buffer_id, data, length);
        }

        return VA_STATUS_SUCCESS;
}

static VAStatus
po_vaapi_update_sei_param(po_vaapi_enc264 *encoder)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;

        /* If we're not using SEI for this encoding mode */
        if ((VA_INVALID_ID == encoder->packed_sei_buf_id) &&
                (VA_RC_CBR != encoder->params.rate_control_method))
        {
                return VA_STATUS_SUCCESS;
        }

        CHECK_VASTATUS(status, po_vaapi_build_packed_sei,
                &encoder->bitstream_sei, &encoder->params);

        encoder->packed_header_params.bit_length = 
                encoder->bitstream_sei->bit_offset;

        CHECK_VASTATUS(status, po_vaapi_update_buffer, encoder->po_context,
                encoder->packed_sei_header_buf_id,
                &encoder->packed_header_params,
                sizeof(VAEncPackedHeaderParameterBuffer));

        CHECK_VASTATUS(status, po_vaapi_update_buffer, encoder->po_context,
                encoder->packed_sei_buf_id,
                encoder->bitstream_sei->buffer,
                ROUND_TO_MULT(encoder->bitstream_sei->bit_offset, 8));

        return VA_STATUS_SUCCESS;
}

VAStatus
po_vaapi_enc264_frame_done(po_vaapi_enc264 *encoder, po_vaapi_enc264_req *req)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;
        if ((NULL == encoder) || (NULL == encoder->po_context) || (NULL == req))
        {
                return VA_STATUS_ERROR_INVALID_VALUE;
        }

        CHECK_VASTATUS(status, vaUnmapBuffer, encoder->po_context->va_display,
                req->output_buffer);
        return VA_STATUS_SUCCESS;
}

VAStatus
po_vaapi_enc264_frame_alloc(po_vaapi_enc264 *encoder,
        VAImage *image, void **out)
{
    VAStatus status = VA_STATUS_ERROR_UNKNOWN;

    /* return the unused surface */
    if ((NULL == encoder) || (NULL == encoder->po_context) 
            || (NULL == image) || (NULL == out))
    {
        return VA_STATUS_ERROR_INVALID_VALUE;
    }

#if 1
    if (IDX_INPUT_0 == encoder->current_input_surface) {
        encoder->current_input_surface = IDX_INPUT_1;
    }
    else {
        encoder->current_input_surface = IDX_INPUT_0;
    }
#endif

    CHECK_VASTATUS(status, vaDeriveImage, encoder->po_context->va_display,
        encoder->va_surfaces[encoder->current_input_surface], image);
    CHECK_VASTATUS(status, vaMapBuffer, encoder->po_context->va_display,
        image->buf, out);

    return VA_STATUS_SUCCESS;
}

static VAStatus
po_vaapi_update_slice_params(po_vaapi_enc264 *encoder,
        enum slice_type pic_type)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;

        encoder->slice_params = (VAEncSliceParameterBufferH264) {
                .num_macroblocks = encoder->params.width_in_mbs
                        * encoder->params.height_in_mbs,
                .slice_type = ((SLICE_TYPE_IDR == pic_type) ? SLICE_TYPE_I : pic_type),
                .slice_alpha_c0_offset_div2 = 2,
                .slice_beta_offset_div2 = 2,
        };

        if ((SLICE_TYPE_P == pic_type) || (SLICE_TYPE_B == pic_type)) {
            int i = 0;
            encoder->slice_params.RefPicList0[0].picture_id = encoder->va_surfaces[IDX_REF_L0];
            for (i = 0; i < 32; i++) {
                encoder->slice_params.RefPicList0[i].picture_id = VA_INVALID_SURFACE;
                encoder->slice_params.RefPicList0[i].flags = VA_PICTURE_H264_INVALID;
            }
        }

        if (SLICE_TYPE_B == pic_type) {
            int i = 0;
            encoder->slice_params.RefPicList1[0].picture_id = encoder->va_surfaces[IDX_REF_L1];
            for (i = 0; i < 32; i++) {
                encoder->slice_params.RefPicList1[i].picture_id = VA_INVALID_SURFACE;
                encoder->slice_params.RefPicList1[i].flags = VA_PICTURE_H264_INVALID;
            }
        }

        CHECK_VASTATUS(status, po_vaapi_update_buffer,
                encoder->po_context, encoder->slice_param_buf_id,
                &encoder->slice_params, sizeof(VAEncSliceParameterBufferH264));

        return VA_STATUS_SUCCESS;
}

static void
po_vaapi_swap_reference_pictures(po_vaapi_enc264 *encoder,
        enum slice_type pic_type, unsigned next_is_bpic)
{
        VASurfaceID tempID = encoder->va_surfaces[IDX_RECON];
        if (pic_type != SLICE_TYPE_B) {
            if (next_is_bpic) {
                encoder->va_surfaces[IDX_RECON] = encoder->va_surfaces[IDX_REF_L1];
                encoder->va_surfaces[IDX_REF_L1] = tempID;
            } else {
                encoder->va_surfaces[IDX_RECON] = encoder->va_surfaces[IDX_REF_L0];
                encoder->va_surfaces[IDX_REF_L0] = tempID;
            }
        } else {
            if (!next_is_bpic) {
                encoder->va_surfaces[IDX_RECON] = encoder->va_surfaces[IDX_REF_L0];
                encoder->va_surfaces[IDX_REF_L0] = encoder->va_surfaces[IDX_REF_L1];
                encoder->va_surfaces[IDX_REF_L1] = tempID;
            }
        }
}

static VAStatus
po_vaapi_setup_pps_sps(po_vaapi_enc264 *encoder, unsigned frame_number,
        unsigned last_frame, enum slice_type pic_type, unsigned next_is_bpic)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;
        po_vaapi_enc264_params *params = &encoder->params;

        if (0 == frame_number) {
                encoder->seq_params = (VAEncSequenceParameterBufferH264) {
                        .level_idc = params->level_idc,
                        .intra_period = params->intra_period,
                        .intra_idr_period = params->intra_period,
                        .max_num_ref_frames = 4,

                        .bits_per_second = params->target_bitrate,
                        .picture_width_in_mbs = params->width_in_mbs,
                        .picture_height_in_mbs = params->height_in_mbs,

                        .time_scale = params->time_scale,
                        .num_units_in_tick = params->num_units_in_tick,

                        .seq_fields = {
                                .bits = {
                                        .frame_mbs_only_flag = 1,
                                        .log2_max_pic_order_cnt_lsb_minus4 = 2,
                                },
                        },

                        .vui_parameters_present_flag = (params->target_bitrate > 0),
                };

                unsigned frame_w_mbaligned = MB_ALIGN(params->width);
                unsigned frame_h_mbaligned = MB_ALIGN(params->height);
                if ((frame_w_mbaligned - params->width) ||
                        (frame_h_mbaligned - params->height))
                {
                        encoder->seq_params.frame_cropping_flag = 1;
                        encoder->seq_params.frame_crop_right_offset =
                                (frame_w_mbaligned - params->width) / 2;
                        encoder->seq_params.frame_crop_bottom_offset =
                                (frame_h_mbaligned - params->height) / 2;
                }

                CHECK_VASTATUS(status, po_vaapi_build_sps,
                        &encoder->bitstream_sps, encoder);

                VAEncPackedHeaderParameterBuffer packed_header_param_buffer = {
                        .type = VAEncPackedHeaderSequence,
                        .bit_length = encoder->bitstream_sps->bit_offset,
                };

                CHECK_VASTATUS(status, po_vaapi_create_or_update_buffer,
                        encoder->po_context, encoder->va_context,
                        &encoder->packed_seq_header_param_buf_id,
                        VAEncPackedHeaderParameterBufferType,
                        &packed_header_param_buffer, sizeof(VAEncPackedHeaderParameterBuffer));

                CHECK_VASTATUS(status, po_vaapi_create_or_update_buffer,
                        encoder->po_context, encoder->va_context,
                        &encoder->packed_seq_buf_id,
                        VAEncPackedHeaderDataBufferType,
                        encoder->bitstream_sps->buffer,
                        ROUND_TO_MULT(encoder->bitstream_sps->bit_offset, 8));

                CHECK_VASTATUS(status, po_vaapi_update_buffer,
                        encoder->po_context, encoder->seq_param_buf_id,
                        &encoder->seq_params,
                        sizeof(VAEncSequenceParameterBufferH264));
        }

        encoder->pic_params = (VAEncPictureParameterBufferH264) {
                /* reconstructed (decoded) picture */
                .CurrPic = {
                        .frame_idx = frame_number,
                        .picture_id = encoder->va_surfaces[IDX_RECON],
                        .TopFieldOrderCnt = 2 * params->current_frame,
                },
                .ReferenceFrames = {
                        [0] = {
                                .picture_id = encoder->va_surfaces[IDX_REF_L0],
                        },
                        [1] = {
                                .picture_id = encoder->va_surfaces[IDX_REF_L1],
                        },
                        [2] = {
                                .flags = VA_PICTURE_H264_INVALID,
                                .picture_id = VA_INVALID_SURFACE,
                        },
                },
                .pic_fields = {
                        .bits = {
                                //.idr_pic_flag = (SLICE_TYPE_IDR == pic_type),
                                .reference_pic_flag = (pic_type != SLICE_TYPE_B),
                                .transform_8x8_mode_flag = !!(params->constraint_set_flag & 0x7),
                                .deblocking_filter_control_present_flag = 1,
                                .entropy_coding_mode_flag = !params->use_cavlc,
                        },
                },
                .frame_num = frame_number,
                .coded_buf = encoder->coded_buffer,
                .last_picture = last_frame,
                .pic_init_qp = params->init_qp,
        };

        CHECK_VASTATUS(status, po_vaapi_build_pps,
                &encoder->bitstream_pps, encoder);

        return VA_STATUS_SUCCESS;
}

VAStatus
po_vaapi_enc264_init_sps_pps(po_vaapi_enc264 *encoder,
        po_vaapi_bitstream_t **pps, po_vaapi_bitstream_t **sps)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;
        if ((NULL == encoder) || (NULL == pps) || (NULL == sps)) {
                return VA_STATUS_ERROR_INVALID_VALUE;
        }

        CHECK_VASTATUS(status, po_vaapi_setup_pps_sps, encoder, 0,
                0, SLICE_TYPE_I, 0);

        *pps = encoder->bitstream_pps;
        *sps = encoder->bitstream_sps;

        return VA_STATUS_SUCCESS;
}

static VAStatus
po_vaapi_fill_response(po_vaapi_enc264 *encoder, po_vaapi_enc264_req *req)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;
        VACodedBufferSegment *coded_buffer_segment = NULL;
        po_vaapi_ctx *ctx = encoder->po_context;

        CHECK_VASTATUS(status, vaSyncSurface, ctx->va_display,
                encoder->va_surfaces[encoder->current_input_surface]);

        VASurfaceStatus surface_status = 0;
        CHECK_VASTATUS(status, vaQuerySurfaceStatus, ctx->va_display,
                encoder->va_surfaces[encoder->current_input_surface],
                &surface_status);

        CHECK_VASTATUS(status, vaMapBuffer, ctx->va_display,
                encoder->coded_buffer, (void**)(&coded_buffer_segment));

        if (!coded_buffer_segment) {
            puts("coded_buffer_segment is NULL");
            return VA_STATUS_ERROR_UNKNOWN;
        }

        if (coded_buffer_segment->status & VA_CODED_BUF_STATUS_SLICE_OVERFLOW_MASK) {
            CHECK_VASTATUS(status, vaUnmapBuffer, ctx->va_display,
                req->output_buffer);
            return VA_STATUS_ERROR_UNKNOWN;
        }

        req->output_buffer = encoder->coded_buffer;
        req->output_data = coded_buffer_segment->buf;
        req->output_size = coded_buffer_segment->size;

        return VA_STATUS_SUCCESS;
}

static VAStatus
po_vaapi_encode_frame_internal(po_vaapi_enc264 *encoder, unsigned frame_number,
        unsigned last_frame, enum slice_type pic_type, unsigned next_is_bpic)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;
        enum {
                MAX_BUFFERS = 32,
        };

        VABufferID buffers[MAX_BUFFERS] = {};
        size_t active_buffers = 0;

#define CHECK_BUFFER(id) do {\
        if ((VA_INVALID_ID != id) && (active_buffers < MAX_BUFFERS)) { \
                buffers[active_buffers++] = id; \
        } \
} while (0);

        CHECK_VASTATUS(status, po_vaapi_setup_pps_sps, encoder, frame_number,
                last_frame, pic_type, next_is_bpic);

        if (1) { //0 == frame_number) {
                VAEncPackedHeaderParameterBuffer packed_header_param_buffer = {
                        .type = VAEncPackedHeaderPicture,
                        .bit_length = encoder->bitstream_pps->bit_offset,
                };

                CHECK_VASTATUS(status, po_vaapi_create_or_update_buffer,
                        encoder->po_context, encoder->va_context,
                        &encoder->packed_pic_header_param_buf_id,
                        VAEncPackedHeaderParameterBufferType,
                        &packed_header_param_buffer, sizeof(VAEncPackedHeaderParameterBuffer));

                CHECK_VASTATUS(status, po_vaapi_create_or_update_buffer,
                        encoder->po_context, encoder->va_context,
                        &encoder->packed_pic_buf_id,
                        VAEncPackedHeaderDataBufferType,
                        encoder->bitstream_pps->buffer,
                        ROUND_TO_MULT(encoder->bitstream_pps->bit_offset, 8));
        }

        CHECK_VASTATUS(status, po_vaapi_update_buffer,
                encoder->po_context, encoder->pic_param_buf_id,
                &encoder->pic_params, sizeof(VAEncPictureParameterBufferH264));

        CHECK_VASTATUS(status, po_vaapi_update_sei_param, encoder);

        CHECK_BUFFER(encoder->seq_param_buf_id);
        CHECK_BUFFER(encoder->pic_param_buf_id);

        CHECK_BUFFER(encoder->packed_seq_header_param_buf_id);
        CHECK_BUFFER(encoder->packed_seq_buf_id);
        CHECK_BUFFER(encoder->packed_pic_header_param_buf_id);
        CHECK_BUFFER(encoder->packed_pic_buf_id);

        CHECK_BUFFER(encoder->packed_sei_header_buf_id);
        CHECK_BUFFER(encoder->packed_sei_buf_id);
        CHECK_BUFFER(encoder->misc_parameter_hrd_buf_id);
        CHECK_BUFFER(encoder->slice_param_buf_id);

        CHECK_VASTATUS(status, po_vaapi_update_slice_params,
                encoder, pic_type);

        CHECK_VASTATUS(status, vaBeginPicture,
                encoder->po_context->va_display, encoder->va_context,
                encoder->va_surfaces[encoder->current_input_surface]);

        CHECK_VASTATUS(status, vaRenderPicture, encoder->po_context->va_display,
                encoder->va_context, buffers, active_buffers);

        CHECK_VASTATUS(status, vaEndPicture,
                encoder->po_context->va_display, encoder->va_context);

        po_vaapi_swap_reference_pictures(encoder, pic_type, next_is_bpic);

        return VA_STATUS_SUCCESS;
}

VAStatus
po_vaapi_enc264_frame_encode(po_vaapi_enc264 *encoder, po_vaapi_enc264_req *req)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;
        po_vaapi_enc264_params *params = NULL;

        if ((NULL == encoder) || (NULL == encoder->po_context)
                || (NULL == req) || (NULL == req->input_image))
        {
                return VA_STATUS_ERROR_INVALID_VALUE;
        }

        params = &encoder->params;
        CHECK_VASTATUS(status, vaUnmapBuffer,
                encoder->po_context->va_display, req->input_image->buf);
        CHECK_VASTATUS(status, vaDestroyImage,
                encoder->po_context->va_display, req->input_image->image_id);

        enum slice_type pic_type = SLICE_TYPE_I;

        if ((params->current_frame % params->intra_period) == 0) {
                pic_type = (params->current_frame == 0)
                        ? SLICE_TYPE_IDR : SLICE_TYPE_I;

                CHECK_VASTATUS(status, po_vaapi_encode_frame_internal,
                        encoder,
                        params->current_frame,
                        req->is_last_frame,
                        pic_type,
                        0);
        } else {
                pic_type = SLICE_TYPE_P;
                CHECK_VASTATUS(status, po_vaapi_encode_frame_internal,
                        encoder,
                        params->current_frame,
                        req->is_last_frame,
                        pic_type,
                        0);
        }
        params->current_frame++;

        CHECK_VASTATUS(status, po_vaapi_fill_response, encoder, req);
        req->pic_type = pic_type;

        return VA_STATUS_SUCCESS;
}

VAStatus
po_vaapi_enc264_free(po_vaapi_enc264 *encoder)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;

#define DESTROY_BUFFER(buffer) do { \
        if (buffer != VA_INVALID_ID) { \
                CHECK_VASTATUS(status, vaDestroyBuffer, \
                        encoder->po_context->va_display, buffer); \
        } \
} while (0)

        DESTROY_BUFFER(encoder->slice_param_buf_id);
        DESTROY_BUFFER(encoder->pic_param_buf_id);
        DESTROY_BUFFER(encoder->seq_param_buf_id);
        DESTROY_BUFFER(encoder->misc_parameter_hrd_buf_id);
        DESTROY_BUFFER(encoder->packed_seq_header_param_buf_id);
        DESTROY_BUFFER(encoder->packed_seq_buf_id);
        DESTROY_BUFFER(encoder->packed_pic_header_param_buf_id);
        DESTROY_BUFFER(encoder->packed_pic_buf_id);
        DESTROY_BUFFER(encoder->packed_sei_header_buf_id);
        DESTROY_BUFFER(encoder->packed_sei_buf_id);

        DESTROY_BUFFER(encoder->coded_buffer);

        po_vaapi_bitstream_free(encoder->bitstream_pps);
        po_vaapi_bitstream_free(encoder->bitstream_sps);
        po_vaapi_bitstream_free(encoder->bitstream_sei);

        CHECK_VASTATUS(status, vaDestroyContext,
                encoder->po_context->va_display, encoder->va_context);
        CHECK_VASTATUS(status, vaDestroySurfaces,
                encoder->po_context->va_display, encoder->va_surfaces, N_SURFACES);
        CHECK_VASTATUS(status, vaDestroyConfig,
                encoder->po_context->va_display, encoder->va_config);

#undef DESTROY_BUFFER
        return VA_STATUS_SUCCESS;
}

int
po_vaapi_enc264_params_compatible(po_vaapi_enc264 *encoder,
        po_vaapi_enc264_params *params)
{
        if ((NULL == encoder) || (NULL == params)) {
                return 0;
        }
#define CHECK(field) if (params->field != encoder->params.field) { return 0; }

        /* maybe some fields from po_vaapi_enc264_params should be
         * moved to po_vaapi_enc264 so that we can directly
         * compare structs
         */

        CHECK(va_profile);
        CHECK(rate_control_method);
        CHECK(width);
        CHECK(height);
        CHECK(use_cavlc);
        CHECK(level_idc);
        CHECK(init_qp);
        CHECK(target_bitrate);
        CHECK(frame_rate);
        CHECK(intra_period);
        CHECK(time_scale);
        CHECK(num_units_in_tick);

#undef CHECK
        return 1;
}
