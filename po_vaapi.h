#ifndef __PO_VAAPI_H
#define __PO_VAAPI_H

#include <va/va.h>
#include <va/va_enc_h264.h>
#include <va/va_x11.h>

struct po_vaapi_enc264_params;
typedef struct po_vaapi_enc264_params po_vaapi_enc264_params;

struct po_vaapi_enc264;
typedef struct po_vaapi_enc264 po_vaapi_enc264;

struct po_vaapi_enc264_req;
typedef struct po_vaapi_enc264_req po_vaapi_enc264_req;

#include "po_vaapi_bitstream.h"

enum {
        IDX_INPUT_0 = 0,
        IDX_INPUT_1 = 1,
        IDX_REF_L0 = 2,
        IDX_REF_L1 = 3,
        IDX_RECON = 4,
        N_SURFACES = IDX_RECON + 1,

        N_REFERENCE_FRAMES = 2,
        N_REF_LIST_ITEMS = 2,
};

enum slice_type {
        SLICE_TYPE_I = 0,
        SLICE_TYPE_B = 1,
        SLICE_TYPE_P = 2,
        SLICE_TYPE_IDR = 7,
};

enum {
        PO_VAAPI_MAX_PPS_SIZE = 0x100,
};

/******************************************************************************
 * VAAPI Window System Context
 *****************************************************************************/
typedef struct po_vaapi_ctx {
        Display *display;
        VADisplay va_display;
        int va_version_major;
        int va_version_minor;

        unsigned va_format;
} po_vaapi_ctx;

po_vaapi_ctx *po_vaapi_alloc(Display *X11_Display);
void po_vaapi_free(po_vaapi_ctx *ctx);


/******************************************************************************
 * encoding parameters and state
 *****************************************************************************/
struct po_vaapi_enc264_params {
        VAProfile va_profile;
        unsigned rate_control_method; //VA_RC_CQP etc

        unsigned width;
        unsigned height;

        unsigned width_in_mbs;
        unsigned height_in_mbs;

        unsigned use_cavlc;

        unsigned level_idc;
        unsigned init_qp;
        long target_bitrate;

        unsigned frame_rate;
        unsigned intra_period;

        unsigned constraint_set_flag;

        unsigned time_scale;
        unsigned num_units_in_tick;

        /* encoding order */
        unsigned current_frame;

        /* SEI Timings */
        unsigned init_cpb_removal_length;
        unsigned init_cpb_removal_delay;
        unsigned init_cpb_removal_delay_offset;
        unsigned cpb_removal_length;
        unsigned cpb_removal_delay;
        unsigned dpb_output_length;
        unsigned dpb_output_delay;
};

/******************************************************************************
 * encode request. the user fills in the input_image and is_last_frame,
 * the encoder sets up the output_data pointer and output_size
 *****************************************************************************/
struct po_vaapi_enc264_req {
        VAImage *input_image;
        unsigned is_last_frame;

        /* pass-through */
        long i_pts;
        long i_dts;

        /* output, filled by the encoder */
        void *output_data;
        size_t output_size;
        enum slice_type pic_type;

        /* encoder-private */
        VABufferID output_buffer;
};

/******************************************************************************
 * VAAPI Encoder Context private data
 *****************************************************************************/
struct po_vaapi_enc264 {
        po_vaapi_ctx *po_context;
        po_vaapi_enc264_params params;

        VAConfigID va_config;
        VAContextID va_context;
        VASurfaceID va_surfaces[N_SURFACES];

        VABufferID coded_buffer;
        size_t coded_buffer_size;
        unsigned current_input_surface;

        VAEncSequenceParameterBufferH264 seq_params;
        VAEncPictureParameterBufferH264 pic_params;
        VAEncSliceParameterBufferH264 slice_params;
        VAEncPackedHeaderParameterBuffer packed_header_params;

        VABufferID seq_param_buf_id;
        VABufferID pic_param_buf_id;
        VABufferID slice_param_buf_id;
        VABufferID misc_parameter_hrd_buf_id;

        VABufferID packed_seq_header_param_buf_id;
        VABufferID packed_seq_buf_id;
        VABufferID packed_pic_header_param_buf_id;
        VABufferID packed_pic_buf_id;
        VABufferID packed_sei_header_buf_id;
        VABufferID packed_sei_buf_id;

        VAPictureH264 ReferenceFrames[N_REFERENCE_FRAMES];
        VAPictureH264 RefPicList0[N_REF_LIST_ITEMS];
        VAPictureH264 RefPicList1[N_REF_LIST_ITEMS];

        po_vaapi_bitstream_t *bitstream_pps;
        po_vaapi_bitstream_t *bitstream_sps;
        po_vaapi_bitstream_t *bitstream_sei;
};

VAStatus
po_vaapi_enc264_alloc(po_vaapi_enc264 **p_encoder,
        po_vaapi_ctx *context,
        po_vaapi_enc264_params *params);

VAStatus
po_vaapi_enc264_free(po_vaapi_enc264 *encoder);

VAStatus
po_vaapi_enc264_init_sps_pps(po_vaapi_enc264 *encoder,
        po_vaapi_bitstream_t **pps, po_vaapi_bitstream_t **sps); 

VAStatus
po_vaapi_enc264_frame_alloc(po_vaapi_enc264 *encoder,
        VAImage *image, void **out);

VAStatus
po_vaapi_enc264_frame_encode(po_vaapi_enc264 *encoder, po_vaapi_enc264_req *req);

VAStatus
po_vaapi_enc264_frame_done(po_vaapi_enc264 *encoder, po_vaapi_enc264_req *req);

VAStatus
po_vaapi_enc264_fixup_params(po_vaapi_enc264_params *params);

int
po_vaapi_enc264_params_compatible(po_vaapi_enc264 *encoder,
        po_vaapi_enc264_params *params);

#endif
