#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "po_vaapi.h"

#define CHECK_VASTATUS(status, func, ...) do {\
        status = func(__VA_ARGS__); \
        if (status != VA_STATUS_SUCCESS) { \
                fprintf(stderr, "%s:%d: %s failed\n", __func__, __LINE__, #func); \
                exit (-1); \
                return status; \
        }\
} while (0)

/******************************************************************************
 * Test code for pa_vaapi encoder
 *****************************************************************************/

enum {
        FRAME_W = 1920,
        FRAME_H = 816,
        //FRAME_H = 1080,
        FRAME_COUNT = 5000,
};

enum input_mode {
    RANDOM_SURFACE,
    STRIPED_SURFACE,
    FILE_YUV,
};

static enum input_mode input_mode_val = FILE_YUV;
static FILE *input_file = NULL;
static void *input_buffer = NULL;

static VAStatus
prepare_input_pic(po_vaapi_enc264 *encoder, VAImage *image)
{
    VAStatus status = VA_STATUS_ERROR_UNKNOWN;
    void *pbuffer = NULL;
    CHECK_VASTATUS(status, po_vaapi_enc264_frame_alloc, encoder, image, &pbuffer);

    size_t y_size = encoder->params.width * encoder->params.height;
    size_t uv_size = y_size / 4;
    size_t frame_size = (3 * y_size) / 2;
    size_t width = encoder->params.width;

    static int fnum = 0;
    fnum++;

    if ((input_mode_val == FILE_YUV) && (NULL == input_file)) {
        input_file = fopen("input.yuv", "rb");
    }

    if ((input_mode_val == FILE_YUV) && (NULL == input_buffer)) {
        input_buffer = malloc(frame_size);
    }

    if (input_mode_val == FILE_YUV) {
        assert(NULL != input_buffer);
        assert(NULL != input_file);
        fread(input_buffer, frame_size, 1, input_file);


        unsigned char *y_dst = (unsigned char*) (pbuffer + image->offsets[0]);
        unsigned char *u_dst = (unsigned char*) (pbuffer + image->offsets[1]);

        unsigned char *y_src = (unsigned char*) input_buffer;
        unsigned char *u_src = (unsigned char*) (input_buffer + y_size);
        unsigned char *v_src = (unsigned char*) (input_buffer + y_size + uv_size);

        for (int row = 0; row < image->height; row++) {
                memcpy(y_dst, y_src, image->width);
                y_dst += image->pitches[0];
                y_src += width;
        }

        for (int row = 0; row < image->height / 2; row++) {
                for (int col = 0; col < image->width / 2; col++) {
                        u_dst[col * 2] = u_src[col];
                        u_dst[col * 2 + 1] = v_src[col];
                }

                u_dst += image->pitches[1];
                u_src += (width / 2);
                v_src += (width / 2);
        }

        return VA_STATUS_SUCCESS;
    }

    for (int i = 0; i < image->num_planes; i++) {
        int idx = (i == 0) ? 0 : ((i == 1) ? 2 : 1);
        int j = image->pitches[idx] * image->height - 9;
        switch (input_mode_val) {
            case RANDOM_SURFACE:
                while (j >= 0) {
                        ((char*)pbuffer)[image->offsets[idx] + j] = fnum * (rand() & 0x1f);
                        j -= 1;
                }
            break;
            case STRIPED_SURFACE:
                while (j >= 0) {
                        ((char*)pbuffer)[image->offsets[idx] + j] = j & 0xff;
                        j -= 1;
                }
            break;
            default:
            break;
        }
    }

    return VA_STATUS_SUCCESS;
}

static FILE *output_file = NULL;

static VAStatus test_encoder(po_vaapi_enc264 *encoder)
{
        VAStatus status = VA_STATUS_ERROR_UNKNOWN;
        unsigned frame = 0;
        while (frame < FRAME_COUNT) {
                printf("frame %d\n", frame);
                VAImage image;

                po_vaapi_enc264_req req = {
                        .input_image = &image,
                        .is_last_frame = ((FRAME_COUNT - 1) == frame),
                };
                CHECK_VASTATUS(status, prepare_input_pic, encoder, &image);
                CHECK_VASTATUS(status, po_vaapi_enc264_frame_encode, encoder,
                        &req);
                fwrite(req.output_data, req.output_size, 1, output_file);
                CHECK_VASTATUS(status, po_vaapi_enc264_frame_done, encoder,
                        &req);
                frame++;
        }

        return VA_STATUS_SUCCESS;
}

int main(int argc, char **argv) {
        po_vaapi_enc264 *encoder = NULL;
        po_vaapi_enc264_params params = {
                .width = FRAME_W,
                .height = FRAME_H,
                .va_profile = VAProfileH264High,
                .rate_control_method = VA_RC_VBR,
        };

        po_vaapi_ctx *ctx = po_vaapi_alloc(NULL);
        if (!ctx) {
                puts("failed to allocate vaapi context");
                goto fail;
        }

        output_file = fopen("out.h264", "wb");
        if (!output_file) {
                puts("failed to open output file");
                goto fail;
        }
        setvbuf(output_file, 0, _IOFBF, 100 * 1000 * 1000);

        if (po_vaapi_enc264_alloc(&encoder, ctx, &params) != VA_STATUS_SUCCESS) {
                puts("failed to initialize h264 encoder");
                goto fail;
        }

        printf("opened vaapi version %d.%d encoder=%p\n",
                ctx->va_version_major, ctx->va_version_minor, encoder);
        test_encoder(encoder);
        po_vaapi_enc264_free(encoder);

fail:
        if (output_file) {
                fflush(output_file);
                fclose(output_file);
        }
        if (ctx) {
                po_vaapi_free(ctx);
        };
}
