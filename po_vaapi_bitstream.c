#include <stdlib.h>
#include <string.h>
#include "po_vaapi_bitstream.h"

#define CHECK_STATUS(status, expr) do {\
        status = expr; \
        if (status != VA_STATUS_SUCCESS) {\
                return status; \
        } \
} while (0)

enum {
        NAL_REF_IDC_NONE = 0,
        NAL_REF_IDC_LOW = 1,
        NAL_REF_IDC_MEDIUM = 2,
        NAL_REF_IDC_HIGH = 3,

        NAL_NON_IDR = 1,
        NAL_IDR = 5,
        NAL_SPS = 7,
        NAL_PPS = 8,
        NAL_SEI = 8,
};

static unsigned int
va_swap32(unsigned int val)
{
    unsigned char *pval = (unsigned char *)&val;

    return ((pval[0] << 24)     |
            (pval[1] << 16)     |
            (pval[2] << 8)      |
            (pval[3] << 0));
}

static VAStatus
bitstream_start(po_vaapi_bitstream_t *bs)
{
    bs->max_size_in_dword = BITSTREAM_ALLOCATE_STEPPING;
    bs->buffer = calloc(bs->max_size_in_dword * sizeof(int), 1);
    bs->bit_offset = 0;

    return (bs->buffer != 0) ? VA_STATUS_SUCCESS : VA_STATUS_ERROR_ALLOCATION_FAILED;
}

static void
bitstream_end(po_vaapi_bitstream_t *bs)
{
    int pos = (bs->bit_offset >> 5);
    int bit_offset = (bs->bit_offset & 0x1f);
    int bit_left = 32 - bit_offset;

    if (bit_offset) {
        bs->buffer[pos] = va_swap32((bs->buffer[pos] << bit_left));
    }
}

static VAStatus
bitstream_put_ui(po_vaapi_bitstream_t *bs, unsigned int val, int size_in_bits)
{
    int pos = (bs->bit_offset >> 5);
    int bit_offset = (bs->bit_offset & 0x1f);
    int bit_left = 32 - bit_offset;

    if (!size_in_bits)
        return VA_STATUS_SUCCESS;

    bs->bit_offset += size_in_bits;

    if (bit_left > size_in_bits) {
        bs->buffer[pos] = (bs->buffer[pos] << size_in_bits | val);
    } else {
        size_in_bits -= bit_left;
        bs->buffer[pos] = (bs->buffer[pos] << bit_left) | (val >> size_in_bits);
        bs->buffer[pos] = va_swap32(bs->buffer[pos]);

        if (pos + 1 == bs->max_size_in_dword) {
            bs->max_size_in_dword += BITSTREAM_ALLOCATE_STEPPING;
            bs->buffer = realloc(bs->buffer, bs->max_size_in_dword * sizeof(unsigned int));
            if (!bs->buffer) {
                    return VA_STATUS_ERROR_ALLOCATION_FAILED;
            }
        }

        bs->buffer[pos + 1] = val;
    }
    return VA_STATUS_SUCCESS;
}

static VAStatus
bitstream_put_ue(po_vaapi_bitstream_t *bs, unsigned int val)
{
    int size_in_bits = 0;
    int tmp_val = ++val;
    VAStatus status = VA_STATUS_ERROR_UNKNOWN;

    while (tmp_val) {
        tmp_val >>= 1;
        size_in_bits++;
    }

    CHECK_STATUS(status, bitstream_put_ui(bs, 0, size_in_bits - 1));
    CHECK_STATUS(status, bitstream_put_ui(bs, val, size_in_bits));
    return VA_STATUS_SUCCESS;
}

static VAStatus
bitstream_put_se(po_vaapi_bitstream_t *bs, int val)
{
    unsigned int new_val;

    if (val <= 0)
        new_val = -2 * val;
    else
        new_val = 2 * val - 1;

    return bitstream_put_ue(bs, new_val);
}

static VAStatus
bitstream_byte_aligning(po_vaapi_bitstream_t *bs, int bit)
{
    int bit_offset = (bs->bit_offset & 0x7);
    int bit_left = 8 - bit_offset;
    int new_val;

    if (!bit_offset)
        return VA_STATUS_SUCCESS;

    if (bit)
        new_val = (1 << bit_left) - 1;
    else
        new_val = 0;

    return bitstream_put_ui(bs, new_val, bit_left);
}

static VAStatus
rbsp_trailing_bits(po_vaapi_bitstream_t *bs)
{
    VAStatus status = VA_STATUS_ERROR_UNKNOWN;
    CHECK_STATUS(status, bitstream_put_ui(bs, 1, 1));
    CHECK_STATUS(status, bitstream_byte_aligning(bs, 0));
    return VA_STATUS_SUCCESS;
}

static VAStatus
nal_start_code_prefix(po_vaapi_bitstream_t *bs)
{
    return bitstream_put_ui(bs, 0x00000001, 32);
}

static VAStatus
nal_header(po_vaapi_bitstream_t *bs, int nal_ref_idc, int nal_unit_type)
{
    VAStatus status = VA_STATUS_ERROR_UNKNOWN;
    CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1)); /* forbidden_zero_bit: 0 */
    CHECK_STATUS(status, bitstream_put_ui(bs, nal_ref_idc, 2));
    CHECK_STATUS(status, bitstream_put_ui(bs, nal_unit_type, 5));
    return VA_STATUS_SUCCESS;
}

VAStatus
sps_rbsp(po_vaapi_bitstream_t *bs, po_vaapi_enc264 *encoder)
{
    VAStatus status = VA_STATUS_ERROR_UNKNOWN;
    int profile_idc = PROFILE_IDC_BASELINE;
    const VAEncSequenceParameterBufferH264 *seq_param = &encoder->seq_params;
    const po_vaapi_enc264_params *params = &encoder->params;

    if (VAProfileH264High == encoder->params.va_profile) {
        profile_idc = PROFILE_IDC_HIGH;
    }
    else if (VAProfileH264Main == encoder->params.va_profile) {
        profile_idc = PROFILE_IDC_MAIN;
    }

    CHECK_STATUS(status, bitstream_put_ui(bs, profile_idc, 8));               /* profile_idc */
    CHECK_STATUS(status, bitstream_put_ui(bs, !!(params->constraint_set_flag & 1), 1));                         /* constraint_set0_flag */
    CHECK_STATUS(status, bitstream_put_ui(bs, !!(params->constraint_set_flag & 2), 1));                         /* constraint_set1_flag */
    CHECK_STATUS(status, bitstream_put_ui(bs, !!(params->constraint_set_flag & 4), 1));                         /* constraint_set2_flag */
    CHECK_STATUS(status, bitstream_put_ui(bs, !!(params->constraint_set_flag & 8), 1));                         /* constraint_set3_flag */
    CHECK_STATUS(status, bitstream_put_ui(bs, 0, 4));                         /* reserved_zero_4bits */
    CHECK_STATUS(status, bitstream_put_ui(bs, seq_param->level_idc, 8));      /* level_idc */
    CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->seq_parameter_set_id));      /* seq_parameter_set_id */

    if (PROFILE_IDC_HIGH == profile_idc) {
        CHECK_STATUS(status, bitstream_put_ue(bs, 1));        /* chroma_format_idc = 1, 4:2:0 */
        CHECK_STATUS(status, bitstream_put_ue(bs, 0));        /* bit_depth_luma_minus8 */
        CHECK_STATUS(status, bitstream_put_ue(bs, 0));        /* bit_depth_chroma_minus8 */
        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1));     /* qpprime_y_zero_transform_bypass_flag */
        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1));     /* seq_scaling_matrix_present_flag */
    }

    CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->seq_fields.bits.log2_max_frame_num_minus4)); /* log2_max_frame_num_minus4 */
    CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->seq_fields.bits.pic_order_cnt_type));        /* pic_order_cnt_type */

    if (0 == seq_param->seq_fields.bits.pic_order_cnt_type)
        CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->seq_fields.bits.log2_max_pic_order_cnt_lsb_minus4));     /* log2_max_pic_order_cnt_lsb_minus4 */
    else {
            return VA_STATUS_ERROR_INVALID_VALUE;
    }

    CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->max_num_ref_frames));        /* num_ref_frames */
    CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1));                                 /* gaps_in_frame_num_value_allowed_flag */

    CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->picture_width_in_mbs - 1));  /* pic_width_in_mbs_minus1 */
    CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->picture_height_in_mbs - 1)); /* pic_height_in_map_units_minus1 */
    CHECK_STATUS(status, bitstream_put_ui(bs, seq_param->seq_fields.bits.frame_mbs_only_flag, 1));    /* frame_mbs_only_flag */

    if (!seq_param->seq_fields.bits.frame_mbs_only_flag) {
        return VA_STATUS_ERROR_INVALID_VALUE;
    }

    CHECK_STATUS(status, bitstream_put_ui(bs, seq_param->seq_fields.bits.direct_8x8_inference_flag, 1));      /* direct_8x8_inference_flag */
    CHECK_STATUS(status, bitstream_put_ui(bs, seq_param->frame_cropping_flag, 1));            /* frame_cropping_flag */

    if (seq_param->frame_cropping_flag) {
        CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->frame_crop_left_offset));        /* frame_crop_left_offset */
        CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->frame_crop_right_offset));       /* frame_crop_right_offset */
        CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->frame_crop_top_offset));         /* frame_crop_top_offset */
        CHECK_STATUS(status, bitstream_put_ue(bs, seq_param->frame_crop_bottom_offset));      /* frame_crop_bottom_offset */
    }

    if (params->target_bitrate > 0)
    {
        CHECK_STATUS(status, bitstream_put_ui(bs, 1, 1)); /* vui_parameters_present_flag */
        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1)); /* aspect_ratio_info_present_flag */
        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1)); /* overscan_info_present_flag */
        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1)); /* video_signal_type_present_flag */
        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1)); /* chroma_loc_info_present_flag */

        if ((seq_param->num_units_in_tick > 0) && (seq_param->time_scale > 0))
        {
            CHECK_STATUS(status, bitstream_put_ui(bs, 1, 1)); /* timing_info_present_flag */
            {
                CHECK_STATUS(status, bitstream_put_ui(bs, seq_param->num_units_in_tick, 32));
                CHECK_STATUS(status, bitstream_put_ui(bs, seq_param->time_scale, 32));
                CHECK_STATUS(status, bitstream_put_ui(bs, 1, 1));
            }
        }
        else
        {
            CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1)); /* timing_info_present_flag */
        }

        if (params->target_bitrate <= 0)
        {
            CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1));/* nal_hrd_parameters_present_flag */
        }
        else
        {
            CHECK_STATUS(status, bitstream_put_ui(bs, 1, 1)); /* nal_hrd_parameters_present_flag */

            // hrd_parameters
            CHECK_STATUS(status, bitstream_put_ue(bs, 0));    /* cpb_cnt_minus1 */
            CHECK_STATUS(status, bitstream_put_ui(bs, 4, 4)); /* bit_rate_scale */
            CHECK_STATUS(status, bitstream_put_ui(bs, 6, 4)); /* cpb_size_scale */

            CHECK_STATUS(status, bitstream_put_ue(bs, params->target_bitrate - 1)); /* bit_rate_value_minus1[0] */
            CHECK_STATUS(status, bitstream_put_ue(bs, params->target_bitrate*8 - 1)); /* cpb_size_value_minus1[0] */
            CHECK_STATUS(status, bitstream_put_ui(bs, 1, 1));  /* cbr_flag[0] */

            CHECK_STATUS(status, bitstream_put_ui(bs, 23, 5));   /* initial_cpb_removal_delay_length_minus1 */
            CHECK_STATUS(status, bitstream_put_ui(bs, 23, 5));   /* cpb_removal_delay_length_minus1 */
            CHECK_STATUS(status, bitstream_put_ui(bs, 23, 5));   /* dpb_output_delay_length_minus1 */
            CHECK_STATUS(status, bitstream_put_ui(bs, 23, 5));   /* time_offset_length  */
        }
        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1));   /* vcl_hrd_parameters_present_flag */
        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1));   /* low_delay_hrd_flag */

        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1)); /* pic_struct_present_flag */
        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1)); /* bitstream_restriction_flag */
    }
    else
    {
        CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1)); /* vui_parameters_present_flag */
    }

    return rbsp_trailing_bits(bs);     /* rbsp_trailing_bits */
}

VAStatus
pps_rbsp(po_vaapi_bitstream_t *bs, VAEncPictureParameterBufferH264 *pic_param)
{
    VAStatus status = VA_STATUS_ERROR_UNKNOWN;
    CHECK_STATUS(status, bitstream_put_ue(bs, pic_param->pic_parameter_set_id));      /* pic_parameter_set_id */
    CHECK_STATUS(status, bitstream_put_ue(bs, pic_param->seq_parameter_set_id));      /* seq_parameter_set_id */

    CHECK_STATUS(status, bitstream_put_ui(bs, pic_param->pic_fields.bits.entropy_coding_mode_flag, 1));  /* entropy_coding_mode_flag */

    CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1));                         /* pic_order_present_flag: 0 */

    CHECK_STATUS(status, bitstream_put_ue(bs, 0));                            /* num_slice_groups_minus1 */

    CHECK_STATUS(status, bitstream_put_ue(bs, pic_param->num_ref_idx_l0_active_minus1));      /* num_ref_idx_l0_active_minus1 */
    CHECK_STATUS(status, bitstream_put_ue(bs, pic_param->num_ref_idx_l1_active_minus1));      /* num_ref_idx_l1_active_minus1 1 */

    CHECK_STATUS(status, bitstream_put_ui(bs, pic_param->pic_fields.bits.weighted_pred_flag, 1));     /* weighted_pred_flag: 0 */
    CHECK_STATUS(status, bitstream_put_ui(bs, pic_param->pic_fields.bits.weighted_bipred_idc, 2));      /* weighted_bipred_idc: 0 */

    CHECK_STATUS(status, bitstream_put_se(bs, pic_param->pic_init_qp - 26));  /* pic_init_qp_minus26 */
    CHECK_STATUS(status, bitstream_put_se(bs, 0));                            /* pic_init_qs_minus26 */
    CHECK_STATUS(status, bitstream_put_se(bs, 0));                            /* chroma_qp_index_offset */

    CHECK_STATUS(status, bitstream_put_ui(bs, pic_param->pic_fields.bits.deblocking_filter_control_present_flag, 1)); /* deblocking_filter_control_present_flag */
    CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1));                         /* constrained_intra_pred_flag */
    CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1));                         /* redundant_pic_cnt_present_flag */

    /* more_rbsp_data */
    CHECK_STATUS(status, bitstream_put_ui(bs, pic_param->pic_fields.bits.transform_8x8_mode_flag, 1));    /*transform_8x8_mode_flag */
    CHECK_STATUS(status, bitstream_put_ui(bs, 0, 1));                         /* pic_scaling_matrix_present_flag */
    CHECK_STATUS(status, bitstream_put_se(bs, pic_param->second_chroma_qp_index_offset ));    /*second_chroma_qp_index_offset */

    return rbsp_trailing_bits(bs);
}

static po_vaapi_bitstream_t*
po_vaapi_bitstream_zalloc_if_null(po_vaapi_bitstream_t **out) {
    po_vaapi_bitstream_t *bs = NULL;
    if (NULL == out) {
        return NULL;
    }

    if (*out != NULL) {
        bs = *out;
        memset(bs->buffer, 0, bs->bit_offset);
        bs->bit_offset = 0;
        return bs;
    }

    bs = malloc(sizeof(po_vaapi_bitstream_t));
    if (!bs) {
        return NULL;
    }
    memset(bs, 0, sizeof(po_vaapi_bitstream_t));
    return bs;
}

VAStatus
po_vaapi_build_pps(po_vaapi_bitstream_t **out, po_vaapi_enc264 *encoder)
{
    VAStatus status = VA_STATUS_ERROR_UNKNOWN;
    po_vaapi_bitstream_t *bs = NULL;
    if ((NULL == out) || (NULL == encoder)) {
        return VA_STATUS_ERROR_INVALID_VALUE;
    }

    bs = po_vaapi_bitstream_zalloc_if_null(out);
    if (!bs) {
        return VA_STATUS_ERROR_ALLOCATION_FAILED;
    }

    CHECK_STATUS(status, bitstream_start(bs));
    CHECK_STATUS(status, nal_start_code_prefix(bs));
    CHECK_STATUS(status, nal_header(bs, NAL_REF_IDC_HIGH, NAL_PPS));
    CHECK_STATUS(status, pps_rbsp(bs, &encoder->pic_params));
    bitstream_end(bs);

    *out = bs;
    return VA_STATUS_SUCCESS;
}

VAStatus
po_vaapi_build_sps(po_vaapi_bitstream_t **out, po_vaapi_enc264 *encoder)
{
    VAStatus status = VA_STATUS_ERROR_UNKNOWN;
    po_vaapi_bitstream_t *bs = NULL;
    if ((NULL == out) || (NULL == encoder)) {
        return VA_STATUS_ERROR_INVALID_VALUE;
    }

    bs = po_vaapi_bitstream_zalloc_if_null(out);
    if (!bs) {
        return VA_STATUS_ERROR_ALLOCATION_FAILED;
    }

    CHECK_STATUS(status, bitstream_start(bs));
    CHECK_STATUS(status, nal_start_code_prefix(bs));
    CHECK_STATUS(status, nal_header(bs, NAL_REF_IDC_HIGH, NAL_SPS));
    CHECK_STATUS(status, sps_rbsp(bs, encoder));
    bitstream_end(bs);

    *out = bs;
    return VA_STATUS_SUCCESS;
}

VAStatus
po_vaapi_build_packed_sei(po_vaapi_bitstream_t **out, po_vaapi_enc264_params *params)
{
    VAStatus status = VA_STATUS_ERROR_UNKNOWN;
    po_vaapi_bitstream_t *bs = NULL;
    size_t bp_byte_size = 0, pic_byte_size = 0;
    int i = 0;
    unsigned char *byte_buf = NULL;
    if ((NULL == out) || (NULL == params)) {
        return VA_STATUS_ERROR_INVALID_VALUE;
    }

    bs = po_vaapi_bitstream_zalloc_if_null(out);
    if (!bs) {
        return VA_STATUS_ERROR_ALLOCATION_FAILED;
    }

    po_vaapi_bitstream_t sei_bp_bs = {}, sei_pic_bs = {};
    CHECK_STATUS(status, bitstream_start(&sei_bp_bs));
    CHECK_STATUS(status, bitstream_put_ue(&sei_bp_bs, 0));       /*seq_parameter_set_id*/
    CHECK_STATUS(status, bitstream_put_ui(&sei_bp_bs,
        params->init_cpb_removal_delay * params->current_frame,
        params->cpb_removal_length));
    CHECK_STATUS(status, bitstream_put_ui(&sei_bp_bs,
        params->init_cpb_removal_delay_offset, params->cpb_removal_length));
    if (sei_bp_bs.bit_offset & 0x7) {
        CHECK_STATUS(status, bitstream_put_ui(&sei_bp_bs, 1, 1));
    }
    bitstream_end(&sei_bp_bs);
    bp_byte_size = (sei_bp_bs.bit_offset + 7) / 8;

    CHECK_STATUS(status, bitstream_start(&sei_pic_bs));
    CHECK_STATUS(status, bitstream_put_ui(&sei_pic_bs,
        params->cpb_removal_delay, params->cpb_removal_length));
    CHECK_STATUS(status, bitstream_put_ui(&sei_pic_bs,
        params->dpb_output_delay, params->dpb_output_length));
    if (sei_pic_bs.bit_offset & 0x7) {
        CHECK_STATUS(status, bitstream_put_ui(&sei_pic_bs, 1, 1));
    }
    bitstream_end(&sei_pic_bs);
    pic_byte_size = (sei_pic_bs.bit_offset + 7) / 8;

    CHECK_STATUS(status, bitstream_start(bs));
    CHECK_STATUS(status, nal_start_code_prefix(bs));
    CHECK_STATUS(status, nal_header(bs, NAL_REF_IDC_NONE, NAL_SEI));

    /* Write the SEI buffer period data */
    CHECK_STATUS(status, bitstream_put_ui(bs, 0, 8));
    CHECK_STATUS(status, bitstream_put_ui(bs, bp_byte_size, 8));

    byte_buf = (unsigned char *)sei_bp_bs.buffer;
    for (i = 0; i < bp_byte_size; i++) {
        CHECK_STATUS(status, bitstream_put_ui(bs, byte_buf[i], 8));
    }
    free(byte_buf);

    /* write the SEI timing data */
    CHECK_STATUS(status, bitstream_put_ui(bs, 0x01, 8));
    CHECK_STATUS(status, bitstream_put_ui(bs, pic_byte_size, 8));

    byte_buf = (unsigned char *)sei_pic_bs.buffer;
    for (i = 0; i < pic_byte_size; i++) {
        CHECK_STATUS(status, bitstream_put_ui(bs, byte_buf[i], 8));
    }
    free(byte_buf);

    rbsp_trailing_bits(bs);

    bitstream_end(bs);

    *out = bs;
    return VA_STATUS_SUCCESS;
}

void
po_vaapi_bitstream_free(po_vaapi_bitstream_t *bs) {
    if (!bs) {
        return;
    }
    if (bs->buffer) {
        free(bs->buffer);
    }
    free(bs);
}

/* vim:set sw=4 */
