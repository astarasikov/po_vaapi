all:
	gcc -std=c99 -O0 -g test_va.c po_vaapi.c po_vaapi_bitstream.c -o myva -lva -lva-x11 -lX11 -Wall -Werror

clean:
	rm *.o
	rm ./myva
