#ifndef __PO_VA_BITSTREAM_H
#define __PO_VA_BITSTREAM_H

struct po_vaapi_bitstream;
typedef struct po_vaapi_bitstream po_vaapi_bitstream_t;
#include "po_vaapi.h"

#define PROFILE_IDC_BASELINE    66
#define PROFILE_IDC_MAIN        77
#define PROFILE_IDC_HIGH        100

#define BITSTREAM_ALLOCATE_STEPPING     4096

struct po_vaapi_bitstream {
    unsigned int *buffer;
    size_t bit_offset;
    size_t max_size_in_dword;
};

VAStatus
po_vaapi_build_pps(po_vaapi_bitstream_t **out, po_vaapi_enc264 *encoder);

VAStatus
po_vaapi_build_sps(po_vaapi_bitstream_t **out, po_vaapi_enc264 *encoder);

VAStatus
po_vaapi_build_packed_sei(po_vaapi_bitstream_t **out, po_vaapi_enc264_params *params);

void po_vaapi_bitstream_free(po_vaapi_bitstream_t *bs);

#endif
